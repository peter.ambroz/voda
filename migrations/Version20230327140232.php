<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230327140232 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add weather table with BA entry';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE weather (id INT AUTO_INCREMENT NOT NULL, location VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, future_status VARCHAR(255) DEFAULT NULL, admin_status VARCHAR(255) DEFAULT NULL, ts DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL, available_at DATETIME NOT NULL, delivered_at DATETIME DEFAULT NULL, INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql("INSERT INTO weather SET location='sk_ba', status='NEJEBE', ts=NOW()");
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE weather');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
