<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230328121650 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'index on locations';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE INDEX idx_location ON weather (location)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX idx_location ON weather');
    }
}
