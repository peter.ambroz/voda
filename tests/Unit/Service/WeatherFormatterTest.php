<?php

declare(strict_types=1);

namespace App\Tests\Unit\Service;

use App\Entity\Weather;
use App\Service\WeatherFormatter;
use PHPUnit\Framework\TestCase;

class WeatherFormatterTest extends TestCase
{
    /**
     * @dataProvider weatherProvider
     */
    public function testFormat(string $status, ?string $future, ?string $adminStatus, string $expected): void
    {
        $w = (new Weather())
            ->setStatus($status)
            ->setFutureStatus($future)
            ->setAdminStatus($adminStatus);

        $this->assertEquals($expected, WeatherFormatter::format($w));
    }

    public function weatherProvider(): iterable
    {
        return [
            'classic' => ['JEBE', 'A AJ BUDE', null, 'JEBE. A AJ BUDE.'],
            'admin_override' => ['JEBE', 'A AJ BUDE', 'NEJEBE', 'NEJEBE'],
            'no_future' => ['JEBE', null, null, 'JEBE.'],
            'no_future_admin_override' => ['JEBE', null, 'NEJEBE', 'NEJEBE'],
        ];
    }
}
