<?php

declare(strict_types=1);

namespace App\Entity;

enum WeatherStatus: string
{
    case SUN = 'NEJEBE';
    case RAIN = 'JEBE';
    case SNOW = 'JEBE SNEH';
}
