<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\WeatherRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: WeatherRepository::class)]
#[ORM\Index(columns: ['location'], name: 'idx_location')]
class Weather
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    private string $location;

    #[ORM\Column(type: 'string', length: 255, enumType: WeatherStatus::class)]
    private WeatherStatus $status;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $futureStatus;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $adminStatus;

    #[ORM\Column]
    private DateTimeImmutable $ts;

    public function __construct(string $location)
    {
        $this->location = $location;
        $this->status = WeatherStatus::SUN;
        $this->futureStatus = null;
        $this->adminStatus = null;
        $this->touch();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function getStatus(): ?WeatherStatus
    {
        return $this->status;
    }

    public function setStatus(WeatherStatus $status): self
    {
        $this->status = $status;
        $this->touch();

        return $this;
    }

    public function getFutureStatus(): ?string
    {
        return $this->futureStatus;
    }

    public function setFutureStatus(?string $futureStatus): self
    {
        $this->futureStatus = $futureStatus;
        $this->touch();

        return $this;
    }

    public function getAdminStatus(): ?string
    {
        return $this->adminStatus;
    }

    public function setAdminStatus(?string $adminStatus): self
    {
        $this->adminStatus = $adminStatus;
        $this->touch();

        return $this;
    }

    public function getTs(): ?DateTimeImmutable
    {
        return $this->ts;
    }

    private function touch(): void
    {
        $this->ts = new DateTimeImmutable();
    }
}
