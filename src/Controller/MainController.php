<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\WeatherRepository;
use App\Service\WeatherFormatter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    #[Route(path: '/{location}', name: 'main', methods: ['GET'])]
    public function main(WeatherRepository $weatherRepository, string $location = 'sk_ba'): Response
    {
        $w = $weatherRepository->findOneBy(['location' => $location]);
        if ($w === null) {
            $verdict = 'NVM. Kde si?';
        } else {
            $verdict = WeatherFormatter::format($w);
        }

        return $this->render('index.html.twig', ['verdict' => $verdict]);
    }
}
