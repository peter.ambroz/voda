<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Weather;
use App\Repository\WeatherRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/chskym/weather')]
class AdminController extends AbstractController
{
    #[Route(path: '/form/{location}', methods: ['GET'])]
    public function form(WeatherRepository $weatherRepository, string $location = 'sk_ba'): Response
    {
        $locations = array_map(fn ($x) => $x['location'], $weatherRepository->getLocations());
        $updateUrl = $this->generateUrl('admin_update', ['location' => 'xx_xx']);
        $readUrl = $this->generateUrl('admin_read', ['location' => 'xx_xx']);
        $backUrl = $this->generateUrl('main', ['location' => 'xx_xx']);

        return $this->render('admin/weather.html.twig', [
            'locations' => $locations,
            'selected' => $location,
            'updateUrl' => $updateUrl,
            'readUrl' => $readUrl,
            'backUrl' => $backUrl,
        ]);
    }

    #[Route(path: '/{location}', name: 'admin_read', methods: ['GET'])]
    public function readWeather(WeatherRepository $weatherRepository, string $location): Response
    {
        $w = $weatherRepository->findOneBy(['location' => $location]);
        if ($w === null) {
            throw new NotFoundHttpException();
        }

        return new JsonResponse(['adminStatus' => $w->getAdminStatus()]);
    }

    #[Route(path: '/{location}', name: 'admin_update', requirements: ['location' => '\w\w_\w+'], methods: ['POST'])]
    public function updateWeather(
        Request $request,
        WeatherRepository $weatherRepository,
        string $location = 'sk_ba',
    ): Response {
        $responseCode = 200;
        $w = $weatherRepository->findOneBy(['location' => $location]);

        if ($w === null) {
            $w = new Weather($location);
            $responseCode = 201;
        }

        $data = json_decode($request->getContent(), true);

        if (array_key_exists('adminStatus', $data)) {
            $w->setAdminStatus($data['adminStatus']);
        }

        $weatherRepository->save($w, true);

        return new Response('OKI DOKI', $responseCode);
    }
}
