<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Weather;

class WeatherFormatter
{
    public static function format(Weather $w): string
    {
        $verdict = $w->getAdminStatus();

        if ($verdict === null) {
            $future = '';

            if ($w->getFutureStatus() !== null) {
                $future = sprintf(" %s.", $w->getFutureStatus());
            }

            $verdict = sprintf("%s.%s", $w->getStatus()->value, $future);
        }

        return $verdict;
    }
}
